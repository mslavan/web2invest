var path    = require('path'),
    webpack = require('webpack');

module.exports = {
  
  devtool: 'cheap-module-eval-source-map',

  entry: [
    'babel-polyfill',
    './client/index',
  ],

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },

  plugins:[

  ],

  module: {

    preLoaders: [
      {
        test: /\.js$/,
        loaders: ['eslint'],
        include: [
          path.resolve(__dirname, 'client')
        ],
      }
    ],

    loaders: [
      {
        test: /\.js$/,        
        include: [
          path.resolve(__dirname, 'client'),
        ],
        exclude: [/node_modules/],
        loaders: ['react-hot', 'babel-loader', 'babel'],
      },
      { 
        test: /\.css$/, 
        exclude: [/node_modules/],
        loader: "style-loader!css-loader", 
      },
    ],

  },
}