let promise = require('bluebird');
let jwt     = require('jsonwebtoken');
let options = { promiseLib: promise };
let pgp = require('pg-promise')(options);

// db config

let db = pgp({
    host: 'localhost',
    port: 5432,
    database: 'investme',
    user: 'postgres',
    password: ''
});


//token config
let tokenConfig = {
  'secret': 'ngEurope rocks!',
  'audience': 'nodejs-jwt-auth',
  'issuer': 'https://gonto.com'
}

module.exports = {
  db,
  tokenConfig,
}