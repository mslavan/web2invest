var express = require('express');
var router = express.Router();

//////////////////////
// Postgres queries
//////////////////////


const auth     	= require('./queries/auth'),
      register 	= require('./queries/register'),
      account 	= require('./queries/account'),
      stories 	= require('./queries/stories'),
      projects 	= require('./queries/projects');


router.get('/api/auth', auth.getAuth);

router.post('/api/register/complete', register.addUser);
router.get('/api/register/check/email', register.getEmailFree);

router.get('/api/stories/one-author', stories.getStoriesOneAuthor);
router.get('/api/stories/:id', stories.getStoryPageData);
router.get('/api/stories/:id/similar', stories.getSimilarStories);
router.get('/api/blog/:id', stories.getBlogPageData);
router.get('/api/stories/authors/all', stories.getStoriesAllAuthors);
router.post('/api/stories/one-author/new', stories.postStory);
router.post('/api/stories/delete', stories.deleteStory);

router.get('/api/projects', projects.getAllProjects);
router.get('/api/project/:id', projects.getProjectPage);
router.get('/api/projects/own', projects.getOwnProjects);
router.post('/api/projects/new', projects.postProject);
router.post('/api/projects/delete', projects.deleteProject);

router.put('/api/account/update', account.updateAccount);
router.get('/api/users', account.getAllUsers);
router.get('/api/users/:id/preview', account.getPreviewUserData);

// ADMIN *******************

router.get('/api/root/auth', auth.getAdminAuth);

router.get('/api/root/blog', stories.getAdminArticles);
router.get('/api/root/blog/top3', stories.getAdminArticlesTop3);
router.post('/api/root/blog/add', stories.postAdminArticle);
router.post('/api/root/blog/delete', stories.deleteAdminArticle);




module.exports = router;