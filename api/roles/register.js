let promise = require('bluebird'),
    jwt     = require('jsonwebtoken'),
    axios   = require('axios');


// db fonfig

const db = require(`../config`).db
const tokenConfig = require(`../config`).tokenConfig

// tokens functions

function createIdToken(user) {
  return jwt.sign(user, tokenConfig.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  return jwt.sign({
    iss: tokenConfig.issuer,
    aud: tokenConfig.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, tokenConfig.secret);
}

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  
  return jti;
}

/////////////////////////////////////////////////////




function getEmailFree(req, res, next) {

  let query =  `select email 
                from users u 
                where u.email = $1`,

      params= [req.query.email]

  db.one(query, params)
    .then(function (data) {
      res.status(202)
        .send({
          errorEmail: true,
        });
    })
    .catch(function (err) {
      console.log(err)
      res.status(200)
        .send({
          errorEmail: false,
      });
    })
}


function addUser(req, res, next) {

  let query =  `INSERT INTO users (email,name,last_name, password, registered) VALUES ($1,$2,$3,$4, NOW()::timestamp) RETURNING *`

  let params= [req.body.email, req.body.name, req.body.last_name, req.body.password]

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          status: 'success',
          id_token: createIdToken(data),
          access_token: createAccessToken()
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}

module.exports = {
  getEmailFree,
  addUser,
}