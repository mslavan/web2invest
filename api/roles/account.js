let promise = require('bluebird'),
    jwt     = require('jsonwebtoken'),
    axios   = require('axios');


// db fonfig

const db = require(`../config`).db
const tokenConfig = require(`../config`).tokenConfig

// tokens functions

function createIdToken(user) {
  return jwt.sign(user, tokenConfig.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  return jwt.sign({
    iss: tokenConfig.issuer,
    aud: tokenConfig.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, tokenConfig.secret);
}

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  
  return jti;
}

/////////////////////////////////////////////////////




function updateAccount(req, res, next) {

  let query =  `UPDATE users u
                SET name = $2,
                  last_name = $3,
                  email = $4,
                  password = $5
                WHERE u.id = $1;

                SELECT * FROM users WHERE id = $1;`

  let params= [req.body.user_id,req.body.name, req.body.last_name, req.body.email, req.body.password]

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          status: 'success',
          id_token: createIdToken(data),
          access_token: createAccessToken()
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getAllUsers(req, res, next) {

  let query =  `SELECT * FROM users`

  let params= []

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data: data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getPreviewUserData(req, res, next) {

  let query =  `SELECT * FROM users WHERE id = $1`

  let params= [req.params.id]

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data: data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}



module.exports = {
  updateAccount,
  getAllUsers,
  getPreviewUserData,
}