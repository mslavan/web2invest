let promise = require('bluebird'),
    jwt     = require('jsonwebtoken'),
    axios   = require('axios');


// db fonfig

const db = require(`../config`).db
const tokenConfig = require(`../config`).tokenConfig

/////////////////////////////////////////////////////




function getStoriesOneAuthor(req, res, next) {

  let query =  `SELECT * FROM articles a WHERE a.author_id = $1 ORDER BY created DESC`

  let params= [req.query.author_id]

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getSimilarStories(req, res, next) {

  let query =  `SELECT * FROM articles a
                WHERE a.a_id != $1  
                ORDER BY RANDOM()
                LIMIT 3`

  let params= [req.params.id]

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getStoriesAllAuthors(req, res, next) {

  const data  = req.query,
        params = [];

  let query =  `SELECT a.*,
                       (name || ' ' || last_name) full_name,
                       to_char(created,'DD.MM.YYYY HH24:MI') created, 
                       (NOW()::timestamp - created ) from_now 
                FROM articles a
                JOIN users u ON u.id = a.author_id
                ${ data.noAnonims === 'true' ? `WHERE a.anonim != 1` : '' }
                ORDER BY a.created DESC`;

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function postStory(req, res, next) {

  let query =  `INSERT INTO articles (author_id,text,title,created,anonim,category_id,image) VALUES ($1,$2,$3, NOW()::timestamp, $4,$5,$6)`

  let params= [req.body.author_id, req.body.text, req.body.title, req.body.anonim, req.body.category, req.body.image]

  db.none( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function deleteStory(req, res, next) {

  let query =  `DELETE FROM articles a WHERE a.a_id = $1`

  let params= [req.body.article_id]

  db.none( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getStoryPageData(req, res, next) {

  let query =  `SELECT a.*,
                       (name || ' ' || last_name) full_name,
                       to_char(created,'DD.MM.YYYY HH24:MI') created, 
                       (NOW()::timestamp - created ) from_now 
                FROM articles a
                JOIN users u ON u.id = a.author_id
                WHERE a.a_id = $1`

  let params= [req.params.id]

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getAdminArticles(req, res, next) {

  let query =  `SELECT * FROM blog_admin ORDER BY b_id DESC`

  db.any(query)
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getAdminArticlesTop3(req, res, next) {

  let query =  `SELECT * FROM blog_admin ORDER BY b_id DESC LIMIT 3`

  db.any(query)
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function postAdminArticle(req, res, next) {

  let query =  `INSERT INTO blog_admin (text, title, created,theme_ids,image) VALUES ($1,$2, NOW()::timestamp, $3,$4)`

  let params= [req.body.text, req.body.title, req.body.categories, req.body.photo]

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}

function deleteAdminArticle(req, res, next) {

  let query =  `DELETE FROM blog_admin WHERE b_id = $1`

  let params= [req.body.b_id]

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getBlogPageData(req, res, next) {

  let query =  `SELECT to_char(created,'DD.MM.YYYY HH24:MI') created, 
                       (NOW()::timestamp - created ) from_now,
                       b.*
                FROM blog_admin b
                WHERE b.b_id = $1`

  let params= [req.params.id]

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}



module.exports = {
  getStoriesOneAuthor,
  getStoriesAllAuthors,
  getSimilarStories,
  postStory,
  deleteStory,
  getStoryPageData,
  getAdminArticles,
  getAdminArticlesTop3,
  postAdminArticle,
  deleteAdminArticle,
  getBlogPageData,
}