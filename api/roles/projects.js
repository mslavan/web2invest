let promise = require('bluebird'),
    jwt     = require('jsonwebtoken'),
    axios   = require('axios');


// db fonfig

const db = require(`../config`).db
const tokenConfig = require(`../config`).tokenConfig

/////////////////////////////////////////////////////



function getAllProjects(req, res, next) {

  const data  = req.query || {},
        params = [];

  console.log(data.noContractPrice)

  let query =  `SELECT p.* ,
                       u.name || ' ' || u.last_name as author
                FROM projects p
                JOIN users u ON u.id = p.author_id
                ${ data.noContractPrice === 'true' ? `WHERE contract_price = FALSE` : '' } 
                `;

  console.log(query)

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getProjectPage(req, res, next) {

  let query =  `SELECT * FROM projects WHERE pr_id = $1 `;

  let params= [req.params.id];

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getOwnProjects(req, res, next) {

  let query =  `SELECT * FROM projects p WHERE p.author_id = $1 ORDER BY created DESC`

  let params= [req.query.author_id]

  db.any( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function postProject(req, res, next) {

  let query =  `INSERT INTO projects (author_id, title, description, currency, amount, contract_price, created) VALUES ($1,$2,$3,$4,$5,$6, NOW()::timestamp)`;

  let params= [req.body.author_id, req.body.title, req.body.description, req.body.currency, req.body.amount, req.body.isContractPrice];

  db.none( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function deleteProject(req, res, next) {

  let query =  `DELETE FROM projects WHERE pr_id = $1`

  let params= [req.body.project_id]

  db.none( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          data,
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}





module.exports = {
  getAllProjects,
  getProjectPage,
  getOwnProjects,
  postProject,
  deleteProject,
}