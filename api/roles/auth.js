let promise = require('bluebird'),
    jwt     = require('jsonwebtoken'),
    axios   = require('axios');


// db fonfig

const db = require(`../config`).db
const tokenConfig = require(`../config`).tokenConfig

// tokens functions

function createIdToken(user) {
  return jwt.sign(user, tokenConfig.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  return jwt.sign({
    iss: tokenConfig.issuer,
    aud: tokenConfig.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, tokenConfig.secret);
}

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  
  return jti;
}

/////////////////////////////////////////////////////




function getAuth(req, res, next) {

  let query =  `SELECT * FROM users u WHERE u.email = $1 AND u.password = $2`

  let params= [req.query.email,req.query.password]

  db.one( query, params )
    .then(function (data) {
      res.status(200)
        .send({
          status: 'success',
          id_token: createIdToken(data),
          access_token: createAccessToken()
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}


function getAdminAuth(req, res, next) {

  let query =  `SELECT * FROM admins a WHERE a.email = $1 AND a.password = $2`

  let params= [req.query.email,req.query.password]

  db.one( query, params )
    .then(function (data) {

      data.root = true;
      
      res.status(200)
        .send({
          status: 'success',
          id_token: createIdToken(data),
          access_token: createAccessToken()
        });
    })
    .catch(function (err) {
       console.log(err)
       next(err)
    })
}



module.exports = {
  getAuth,
  getAdminAuth,
}