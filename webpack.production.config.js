const path = require('path');
const webpack = require('webpack');


module.exports = {
  entry: [
    'babel-polyfill',
    './client/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.min.js',
    publicPath: '/',
  },

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    /*new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
        screw_ie8: true,
        drop_console: true,
        drop_debugger: true,
      },
    }),*/
  ],
  module: {

    preLoaders: [
      {
        test: /\.js$/,
        loaders: ['eslint'],
        include: [
          path.resolve(__dirname, 'client')
        ],
      }
    ],

    loaders: [
      {
        test: /\.js$/,        
        include: [
          path.resolve(__dirname, 'client'),
        ],
        exclude: [/node_modules/],
        loaders: ['react-hot', 'babel-loader', 'babel'],
      },
      { 
        test: /\.css$/, 
        exclude: [/node_modules/],
        loader: "style-loader!css-loader", 
      },
    ],

  },
};