var webpack               = require('webpack'),
    webpackDevMiddleware  = require('webpack-dev-middleware'),
    webpackHotMiddleware  = require('webpack-hot-middleware'),
    bodyParser            = require('body-parser'),
    express               = require('express'),
    compression           = require('compression'),
    path                  = require('path'),
    config                = require('../webpack.config'),
    port                  = process.env.PORT || 3000;

const app = express();
const compiler = webpack(config);
const isDevelopment  = app.get('env') !== "production";

if (isDevelopment) {
  app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false,
    },
  }));

  app.use(webpackHotMiddleware(compiler));

} else {
  app.use(compression());
}

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(express.static(path.join(__dirname, '..', 'dist')));

var api = require('../api/index')
app.use(api)

app.get("/robots.txt", function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','/robots.txt'));
})

app.get("/sitemap.xml", function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','/sitemap.xml'));
})

app.get("/public/images/logo-big.png", function(req, res, next) {
  res.sendFile(path.join(__dirname, '..','public/images/logo-big.png'));
})

app.get('*', require('../client').serverMiddleware);


app.listen(port,function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port )
  }
})