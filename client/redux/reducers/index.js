import { combineReducers } from 'redux'

import profile from './Profile'
import asyncData from './AsyncData'

const quotesApp = combineReducers({
	asyncData,
    profile,
});

export default quotesApp