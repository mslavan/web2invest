let cookieOptions = {
    path: '/',
    maxAge: 5184000
};

const initialState = {
	user : {},
};


export default function profile(state = initialState , action ) {
	switch (action.type) {
		case 'SET_LOGIN_USER' :
		    return { ...state, user: action.user };

		case 'SET_LOGOUT_USER' :
		    return { ...state, user: action.user };

      default:
		return state;
	}
  return state
}
