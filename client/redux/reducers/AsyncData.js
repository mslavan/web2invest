const initialState = {
	storyData: {},
	blogData: {},
	similarStories: [],
	usersArticles: [],
	adminArticles: [],
	projects: [],
	projectData: [],
	allUsers: [],
	userPreviewData: {},
};

export default function asyncData(state = initialState , action ) {
	switch (action.type) {

	    case 'STORY_PAGE_DATA_LOADED':
	    	return { ...state, storyData: action.storyData };		

	    case 'BLOG_PAGE_DATA_LOADED':
	    	return { ...state, blogData: action.blogData };		

	    case 'GET_SIMILAR_STORIES':
	    	return { ...state, similarStories: action.similarStories };	
	    
	    case 'GET_USERS_ARTICLES_HOME':
	    	return { ...state, usersArticles: action.usersArticles };	
	    
	    case 'GET_ADMIN_ARTICLES_HOME':
	    	return { ...state, adminArticles: action.adminArticles };	    
	    
	    case 'GET_PROJECTS':
	    	return { ...state, projects: action.projects };			    
	    
	    case 'GET_PROJECT_DATA':
	    	return { ...state, projectData: action.projectData };			    
	    
	    case 'GET_USERS_LIST':
	    	return { ...state, allUsers: action.allUsers };			    
	   
	    case 'GET_USERS_PREVIEW_DATA':
	    	return { ...state, userPreviewData: action.userPreviewData };		

      default:
		return state;
	}
  return state
}

