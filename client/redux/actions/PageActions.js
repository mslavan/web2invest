import jwt_decode from 'jwt-decode';
import Cookies from 'universal-cookie';

export const SET_LOGIN_USER = 'SET_LOGIN_USER',
             SET_LOGOUT_USER = 'SET_LOGOUT_USER'
            

const cookies = new Cookies();
let cookieOptions = {
    path: '/'
};
let cookieOptionsBooking = {
    path: '/booking'
};


export function loginUser(value){
    let savedJwt =  cookies.get('InvestUser');

	if (savedJwt !== value) {
        cookies.set('InvestUser', value, {...cookieOptions});
    }

	return {
		type: SET_LOGIN_USER ,
		user: jwt_decode(value)
	}

}

export function logoutUser(){
    cookies.remove('InvestUser', {...cookieOptions});

	return {
		type: SET_LOGOUT_USER,
		user: {}
	}

}

export function updateUser(){
    let savedJwt =  cookies.get('InvestUser');
	
	return {
		type: SET_LOGIN_USER ,
		user: savedJwt ? jwt_decode(savedJwt) : {}
	}	
}
