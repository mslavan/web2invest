import axios from 'axios'
import * as pageActions from './actions/PageActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const isClient = typeof document !== 'undefined';

let DOMAIN = '';

if(isClient){
  DOMAIN = window.location.hostname === 'localhost' ? 'http://localhost:3000' : 'https://web2invest.com';
} else {
  DOMAIN =  global.process.domain === 'web2invest.com' ? 'https://web2invest.com' : 'http://localhost:3000';
}

// OTHER *****************************


export function saveImageRemote (image) {
  return  axios.post('https://api.imgur.com/3/image', {
              image : image,
              type: 'base64'
            },{
                headers: { 
                  Authorization: 'Client-ID 742e78dbe8f441f',
                  Accept: 'application/json'
                }
          })
}


// ADMIN *****************************


export function getAuthRoot (email, password) {
  return  axios.get(DOMAIN + '/api/root/auth', {
            params: {
              email,
              password
            }
          })
}

export function getAdminArticles () {
  return  axios.get(DOMAIN + '/api/root/blog')
}

export function postAdminArticle (categories, text, title, photo) {
  return  axios.post(DOMAIN + '/api/root/blog/add', {
            categories,
            text,
            title,
            photo,
          })
}

export function deleteAdminArticle (b_id) {
  return  axios.post(DOMAIN + '/api/root/blog/delete', {
            b_id,
          })
}


// USER ******************************


function getStoryPageFromAPI(id) {
    return axios
            .get(DOMAIN + `/api/stories/${id}`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getBlogPageFromAPI(id) {
    return axios
            .get(DOMAIN + `/api/blog/${id}`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getSimilarStoriesFromAPI(id) {
    return axios
            .get(DOMAIN + `/api/stories/${id}/similar`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getUsersArticlesHomeFromAPI(params) {
    return axios
            .get(DOMAIN + `/api/stories/authors/all`, { params })
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getAdminArticlesHomeFromAPI() {
    return axios
            .get(DOMAIN + `/api/root/blog/top3`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getProjectDataFromAPI(id) {
    return axios
            .get(DOMAIN + `/api/project/${id}`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getAllProjectsFromAPI(params) {
    return axios
            .get(DOMAIN + `/api/projects`, { params })
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getUsersListFromAPI(params) {
    return axios
            .get(DOMAIN + `/api/users`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}


function getUserPrewiewDataFromAPI(id) {
    return axios
            .get(DOMAIN + `/api/users/${id}/preview`)
            .then( res => res.data )
            .catch( err => console.log(err) );
}







export function isFreeEmail (email) {
	return 	axios.get(DOMAIN + '/api/register/check/email', {
    				params: {
    					email,
    				}
    			})
}

export function addUser (email, name, last_name, password) {
    return axios.post(DOMAIN + '/api/register/complete', {
              email,
              name,
              last_name,
              password,
            })
}


export function getAuth (email, password) {
  return  axios.get(DOMAIN + '/api/auth', {
            params: {
              email,
              password
            }
          })
}


export function postStory (author_id, text, title, anonim, category, image) {
  return  axios.post(DOMAIN + '/api/stories/one-author/new', {
            author_id,
            text,
            title,
            anonim,
            category, 
            image,
          })
}

export function postProject (author_id, title, description, currency, amount, isContractPrice) {
  return  axios.post(DOMAIN + '/api/projects/new', {
            author_id,
            title,
            description,
            currency,
            amount,
            isContractPrice
          })
}


export function getStoriesAllAuthors () {
  return  axios.get(DOMAIN + '/api/stories/authors/all')
}


export function getStoriesOneAuthor (author_id) {
  return  axios.get(DOMAIN + '/api/stories/one-author', {
            params: {
              author_id,
            }
          })
}


export function getOwnProjects (author_id) {
  return  axios.get(DOMAIN + '/api/projects/own', {
            params: {
              author_id,
            }
          })
}


export function updateUserAccount (user_id, name, last_name, email, password) {
  return  axios.put(DOMAIN + '/api/account/update', {
            user_id,
            name,
            last_name,
            email,
            password
          })
}


export function getDeleteStory (article_id) {
  return  axios.post(DOMAIN + '/api/stories/delete', {
            article_id,
          })
}


export function deleteOwnProject (project_id) {
  return  axios.post(DOMAIN + '/api/projects/delete', {
            project_id ,
          })
}


export function getStoryPageData(id) {
    return async function (dispatch, getState) {
        let {data} = await getStoryPageFromAPI(id);
        dispatch({ type: 'STORY_PAGE_DATA_LOADED', storyData: data });
    }
}


export function getBlogPageData(id){
    return async function (dispatch, getState) {
        let {data} = await getBlogPageFromAPI(id);
        dispatch({ type: 'BLOG_PAGE_DATA_LOADED', blogData: data });
    }
}


export function getSimilarStories(id){
    return async function (dispatch, getState) {
        let {data} = await getSimilarStoriesFromAPI(id);
        dispatch({ type: 'GET_SIMILAR_STORIES', similarStories: data });
    }
}


export function getUsersArticlesHome(params = {}){
    return async function (dispatch, getState) {
        let {data} = await getUsersArticlesHomeFromAPI(params);
        dispatch({ type: 'GET_USERS_ARTICLES_HOME', usersArticles: data });
    }
}


export function getAdminArticlesHome(){
    return async function (dispatch, getState) {
        let {data} = await getAdminArticlesHomeFromAPI();
        dispatch({ type: 'GET_ADMIN_ARTICLES_HOME', adminArticles: data });
    }
}


export function getAllProjects(params = {}){
    return async function (dispatch, getState) {
        let {data} = await getAllProjectsFromAPI(params);
        dispatch({ type: 'GET_PROJECTS', projects: data });
    }
}


export function getProjectData(id){
    return async function (dispatch, getState) {
        let {data} = await getProjectDataFromAPI(id);
        dispatch({ type: 'GET_PROJECT_DATA', projectData: data });
    }
}

export function getUsersList(params = {}){
    return async function (dispatch, getState) {
        let {data} = await getUsersListFromAPI(params);
        dispatch({ type: 'GET_USERS_LIST', allUsers: data });
    }
}

export function getUserPrewiewData(id){
    return async function (dispatch, getState) {
        let {data} = await getUserPrewiewDataFromAPI(id);

        console.log(data)
        dispatch({ type: 'GET_USERS_PREVIEW_DATA', userPreviewData: data });
    }
}
