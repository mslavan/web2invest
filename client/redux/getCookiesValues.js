

export default (cookies){
	return {
		profile: {
			user : cookies.get('InvestUser') ? jwt_decode(cookies.get('InvestUser')) : {}
		}
	}
}