import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment';
import {Helmet} from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';

import * as actions from'../redux/axiosFunctions' 

import SharingSocial from '../components/SharingSocial'
import Disqus from '../components/Disqus'

const currencies = require('../allData').currencies;  

const styles = {

  center: {
    textAlign: 'center',
  },

  articleLogo: {
    maxWidth: '100%',
    height: 'auto',
  },

}


class ProjectPage extends PureComponent {
  
  static fetchData({ store, params }) {
    return store.dispatch(actions.getUserPrewiewData(params.id));
  }

  componentDidMount(){
    const paramID = this.props.match.params.id;
    const title = `${this.props.userPreviewData.name} ${this.props.userPreviewData.last_name}` || 'Неизвестный';

    this.props.getUserPrewiewData(paramID);

    this.props.setNavLinks([
      { ref: '/users', title: 'Пользователи' },
      { ref: `/users/${paramID}`, title: title },
    ])
  }

  componentWillReceiveProps(nextProps){
    const paramID = nextProps.match.params.id;
    const title = `${nextProps.userPreviewData.name} ${nextProps.userPreviewData.last_name}` || 'Неизвестный';

    if(nextProps.userPreviewData.id !== this.props.userPreviewData.id){
      this.props.setNavLinks([
        { ref: '/users', title: 'Пользователи' },
        { ref: `/users/${paramID}`, title: title },
      ])
    }
  }

  render() {
    const userPreviewData = this.props.userPreviewData    
    const url  = `https://web2invest.com${this.props.location.pathname}`
    console.log(this.props)

    return(
          <div style={styles.container}>

            <Helmet>
              <title>{`${userPreviewData.name} ${userPreviewData.last_name} - Пользователь web2invest`}</title>

              <meta property="fb:app_id" content="123715308261227"/>
              <meta property="og:title" content={`${userPreviewData.name} ${userPreviewData.last_name} - Пользователь web2invest`}/>
              <meta property="og:description" content="Все пользователи портала web2invest.com"/>
              <meta property="og:type" content="article"/>
              <meta property="og:url" content={url} />
            </Helmet>

            <Grid container spacing={24}>

              <Grid item xs={12}>
                <p>Полное имя: {userPreviewData.name} {userPreviewData.last_name}</p>
                <p>Контакты</p>
                <p>E-mail: {userPreviewData.email}</p>
              </Grid>
            
            </Grid>
          </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        userPreviewData: state.asyncData.userPreviewData,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(ProjectPage);