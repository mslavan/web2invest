import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment';
import {Helmet} from "react-helmet";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'material-ui/Icon';
import AccessTime from 'material-ui-icons/AccessTime'

import * as actions from '../redux/axiosFunctions' 

import ProjectsFilters from '../components/ProjectsFilters'

const axiosFunc = require('../redux/axiosFunctions') 
const currencies = require('../allData').currencies;  

const styles = {

  root: {
    padding: 10,
  },

  cardContainer: {
    marginBottom: 15,
    padding: 15,
    borderRadius: 3,
    background: '#fff',
  },

  price: {
    display: 'inline-block',
    borderRadius: 7,
    marginTop: 10,
    padding: '3px 8px',
    color: '#fff',
    background: '#3f51b5'
  },

  date: {
    display: 'inline-block',
  },

  dateIcon: {
    display: 'inline-block',
    marginTop: 5,
  },
}

class Users extends PureComponent {
  
  static fetchData({ store, params }) {
    return Promise.all([
      store.dispatch(actions.getUsersList()) 
    ])
  }

  constructor(props) {
    super(props);
    
    this.state={
    }
  }

  componentWillMount(){
    

  }

  componentDidMount(){
    this.props.getUsersList();
    this.props.setNavLinks([
      { ref: '/users', title: 'Пользователи' },
    ])  
  }

  applyFilters(params){
    this.props.getUsersList(params) 
  }

	render() {

    const {
      allUsers
    } = this.props

		return(
          <div>
            
            <Helmet>
              <title>Проэкты для фрилансеров на сервисе web2invest</title>
            </Helmet>

            <Grid container spacing={0}>
              
              <Grid item xs={12} md={3} xl={3}>
                <ProjectsFilters update={::this.applyFilters}/>
              </Grid>

              <Grid item xs={12} md={9} xl={9}>
                <div style={styles.root}>
                  <p style={{ paddingLeft: 15 }}>Всего пользователей: {allUsers.length}</p>
                </div>
                <div style={styles.root}>
                {allUsers.length
                  ? allUsers.map( user =>
                      <div key={user.id} style={styles.cardContainer}>
                        <Link to={`/users/${user.id}`}>{user.name} {user.last_name}</Link>
                      </div>
                    )
                  : <div>
                      <p>Ничего не найдено, измените условия поиска</p>
                    </div>
                }
                </div>
              </Grid>

            </Grid>

          </div>
		)
	}
}


function mapStateToProps(state) {
    return {
        allUsers: state.asyncData.allUsers,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Users);