import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import ExpansionPanel, {
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ExpansionPanelActions,
} from 'material-ui/ExpansionPanel';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import {Helmet} from "react-helmet";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SharingSocial from '../components/SharingSocial'
import Disqus from '../components/Disqus'

import * as actions from'../redux/axiosFunctions' 

const styles = {

  container: {
    margin: 'auto',
    maxWidth: 600,
  },
  
  title: {
    textAlign: 'center',
    fontSize: 26,
  },

  articleLogo: {
    maxWidth: '100%',
    height: 'auto',
  },

  text: {
    paddingTop: 15,
  },

  similarLogo: {
    width: 200,
  },
}


class StoryPage extends Component {
  
  static fetchData({ store, params }) {
    return store.dispatch(actions.getStoryPageData(params.id));
  }


  componentDidMount(){
    this.updateAllData(this.props);
    this.updateNavLinks(this.props);   
  }


  componentWillReceiveProps(nextProps){

    if(nextProps.location !== this.props.location){
      this.updateAllData(nextProps);
    }

    if(nextProps.storyData.title !== this.props.storyData.title){
      this.updateNavLinks(nextProps);
    }
  }


  updateAllData(props){

    const paramID = props.match.params.id;
    
    this.props.getStoryPageData(paramID);
    this.props.getSimilarStories(paramID);
    this.updateNavLinks(props);
  }


  updateNavLinks(props){

      props.setNavLinks([
        { ref: '/blog', title: 'Блог' },
        { ref: `/blog/${props.match.params.id}`, title: props.storyData.title },
      ])
  }

	render() {
    
    const { storyData, similarStories } = this.props;
    const url  = `https://web2invest.com${this.props.location.pathname}`;
    const disqusConfig = {
        identifier: 'web2invest',
        url: `https://web2invest.com${this.props.location.pathname}`,
        title: '',      
    }

		return(
        <div style={styles.container}>
            
          <Helmet>
            <title>{`${storyData.title}`}</title>
            <meta property="fb:app_id" content="123715308261227"/>
            <meta property="og:title" content={storyData.title || 'Без темы'}/>
            <meta property="og:description" content={storyData.text && storyData.text.replace(/(<([^>]+)>)/ig," ")}/>
            <meta property="og:image" content={storyData.image}/>
            <meta property="og:type" content="article"/>
            <meta property="og:url" content={url} />
          </Helmet>

          <Grid container spacing={24}>

            <Grid item xs={12} md={2} xl={3}>                         
              <div style={{ marginTop: 55, marginBottom: 25 }}>
                <SharingSocial url={url} isStatic={false} />
              </div> 
            </Grid>

            <Grid item xs={12} md={8} xl={5}>
              <h1 className='heading extra__bold'>{storyData.title}</h1>
              { storyData.image && <img src={storyData.image} style={styles.articleLogo} alt={storyData.image} title={storyData.title}/> }
              <div dangerouslySetInnerHTML={{ __html: storyData.text }} style={styles.text} className='bigger__text'></div>
              
              <div style={{ marginTop: 20, marginBottom: 40 }}>
                  <b className='sec__color'>{ storyData.anonim === 0 ? storyData.full_name : 'Анонимно' }</b>
                  <span style={{ paddingLeft: 15 }}>, {storyData.created}</span>
              </div>

              <p className='largest__text extra__bold'>Другие записи</p>
              <Grid container spacing={24} style={{ marginBottom: 50 }}>
                {similarStories.map( story =>
                  <Grid item xs={12} key={story.id} style={styles.cardContainer}>                   
                    <img src={story.image} alt={story.image} title={story.title}  style={styles.similarLogo}/>
                    <Link to={`/stories/${story.a_id}`}><p className='extra__bold bigger__text link'>{story.title}</p></Link>
                  </Grid>
                )}
              </Grid>

              <Disqus config={disqusConfig} />

            </Grid>

            <Grid item xs={12} md={2} xl={4}>                         
            </Grid>

          </Grid>
        </div>
		)
	}
}

function mapStateToProps(state) {
    return {
        storyData: state.asyncData.storyData,
        similarStories: state.asyncData.similarStories,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(StoryPage);