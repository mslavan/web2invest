import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Stepper, { Step, StepLabel, StepContent } from 'material-ui/Stepper';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Grid from 'material-ui/Grid';
import { bindActionCreators } from 'redux';
import * as pageActions from '../redux/actions/PageActions'
import { connect } from 'react-redux'
import _ from 'lodash'

const axiosFunc = require('../redux/axiosFunctions') 

const regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const styles = theme => ({
  root: {
  	margin: 'auto',
    width: '100%',
    maxWidth: 600,
  },
  button: {
    marginRight: theme.spacing.unit,
  },
  actionsContainer: {
    marginTop: theme.spacing.unit,
    paddingLeft: 30,
    marginBottom: theme.spacing.unit,
  },
  resetContainer: {
    marginTop: 0,
    paddingLeft: 30,
    padding: theme.spacing.unit * 3,
  },
  formContainer: {
  	paddingLeft: 30,
  },
});

const getSteps = () => {
  return ['Общие данные', 'Личные данные'];
}

function mapStateToProps(state) {
  return {
    profile: state.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  }
}

@connect(mapStateToProps, mapDispatchToProps)
class VerticalLinearStepper extends Component {

  constructor(props) {
    super(props);
    
    this.state={
      activeStep: 0,
      name: '',
      lastName: '',
      email: ''    
    }
  }  
 
  componentWillMount(){
    if( !_.isEmpty(this.props.profile && this.props.profile.user) ){
      this.props.history.push('/')
    }
  }

  handleNext(){

    console.log(this.stepDataValid())
    if( !this.stepDataValid() ){
      this.setState({ 
        errorDataStep: true, 
        errorText: 'Проверьте поля',
      })
      return;
    }

    if(this.state.activeStep === 1){

      this.isFreeEmail().then( (response) => {
        if(response.status === 200){
          this.regUser()
          this.setState({
            activeStep: this.state.activeStep + 1,
            errorDataStep: false,
          });
        }else {
          this.setState({ 
            errorDataStep: true,
            errorText: 'Пользователь с такой почтой уже зарегестрирован',
          })
          return;
        }

      })
    } else {
      this.setState({
        activeStep: this.state.activeStep + 1,
        errorDataStep: false,
        errorText: 'Проверьте поля',
      });
    }

  }

  handleBack(){
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  }

  handleReset(){
    this.setState({
      activeStep: 0,
    });
  }

  stepDataValid(){
  	  let isValidStepData = true;

	  switch (this.state.activeStep) {
	    case 0:
	      return isValidStepData = this.state.name.length > 0 && this.state.lastName.length > 0;
	    case 1:
	      return isValidStepData = regExpEmail.test(this.state.email) && this.state.password.length > 0 ;
	    case 2:
	      return isValidStepData = true;

	    default:
	      return true;
	  }  
  }

  isFreeEmail(){ 
    return axiosFunc.isFreeEmail(this.state.email) 
  }

  regUser(){
    axiosFunc
      .addUser(this.state.email,this.state.name,this.state.lastName, this.state.password)  
      .then( (response) => {
        this.props.pageActions.loginUser(response.data.id_token)
        this.props.history.push('/dashboard')
      })  
  }

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;
    console.log(this.props)

    return (
      <div className={classes.root}>
      	<h1>Регистрация</h1>

        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => {
            return (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
                <StepContent>
			      <form className={classes.container} noValidate autoComplete="off">
			        { index === 0 &&
			        	<Grid container className={classes.formContainer}>
				        	<Grid item xs={6}>
					        	<TextField
						          id="name"
						          label="Имя*"
						          value={this.state.name}
						          onChange={(e) => this.setState({ name: e.target.value })}
						          margin="normal"
						        />
						    </Grid>
			        	<Grid item xs={6}>
				        	<TextField
					          id="lastname"
					          label="Фамилия*"
					          value={this.state.lastName}
					          onChange={(e) => this.setState({ lastName: e.target.value })}
					          margin="normal"
					        />
						    </Grid>
						</Grid>
					  || index === 1 &&
                <Grid container className={classes.formContainer}>
                  <Grid item xs={6}>
                    <TextField
                      id="email"
                      label="Емейл*"
                      value={this.state.email}
                      onChange={(e) => this.setState({ email: e.target.value })}
                      margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="password"
                    label="Пароль*"
                    value={this.state.password}
                    onChange={(e) => this.setState({ password: e.target.value })}
                    margin="normal"
                  />
                </Grid>
            </Grid>
					}
			      </form>
                  <div className={classes.actionsContainer}>
                  	{ this.state.errorDataStep && <h3>{this.state.errorText}</h3> }
                    <div>
                      <Button
                        disabled={activeStep === 0}
                        onClick={::this.handleBack}
                        className={classes.button}
                      >
                        Назад
                      </Button>
                      <Button
                        raised
                        color="primary"
                        onClick={::this.handleNext}
                        className={classes.button}
                      >
                        {activeStep === steps.length - 1 ? 'Зарегистрироваться' : 'Дальше'}
                      </Button>
                    </div>
                  </div>
                </StepContent>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>Регистрация успешно завершена!</Typography>
          </Paper>
        )}
      </div>
    );
  }
}

VerticalLinearStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(VerticalLinearStepper);