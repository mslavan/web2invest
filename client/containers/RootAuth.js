import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Stepper, { Step, StepLabel, StepContent } from 'material-ui/Stepper';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Grid from 'material-ui/Grid';
import { bindActionCreators } from 'redux';
import * as pageActions from '../redux/actions/PageActions'
import { connect } from 'react-redux'
import Snackbar from 'material-ui/Snackbar';
import _ from 'lodash'

const axiosFunc = require('../redux/axiosFunctions') 

const styles = {
  root: {
    margin: 'auto',
    width: 'auto',
    maxWidth: 400,
  },

  formContainer: {
    textAlign: 'center',
    padding: 30,
    background: '#fff',
    borderRadius: 3,
    border: '1px solid',
  },

  button: {
    margin: '10px auto',
  }
}


function mapStateToProps(state) {
  return {
    profile: state.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  }
}

@connect(mapStateToProps, mapDispatchToProps)
class Auth extends Component {

  constructor(props) {
    super(props);
    
    this.state={
      email: '',
      password: '',
    }
  }

  componentWillMount(){
    if( !_.isEmpty(this.props.profile && this.props.profile.user) ){
      this.props.history.push('/')
    }
  }

  getAuth(){

    if(this.state.email && this.state.password){
      this.axiosAuth()
    }
  }

  axiosAuth(){
    axiosFunc
      .getAuthRoot(this.state.email,this.state.password)
      .then( (response) => {
        this.props.pageActions.loginUser(response.data.id_token)
        this.props.history.push('/root/dashboard')
      })
      .catch( err => this.setState({ openErrorSnack: true }) )
  } 

  render() {
    const { classes } = this.props;
    console.log(this.props)

    return (
      <div>
        <form className={classes.root} noValidate autoComplete="off">
          <h1>Админ панель</h1>
          <Grid container className={classes.formContainer}>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  id="email"
                  label="Емейл"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  id="password"
                  label="Пароль"
                  value={this.state.password}
                  onChange={(e) => this.setState({ password: e.target.value })}
                  margin="normal"
                />
              <Grid item xs={12}>
                <Button raised color="primary" className={classes.button} onClick={::this.getAuth}>
                  Войти
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </form>

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={this.state.openErrorSnack}
          onClose={() => this.setState({ openErrorSnack: false })}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">Почта или пароль неверны</span>}
          action={ <span style={{ color: '#ff1b1b' }}>ОШИБКА</span>}
        />

      </div>
    );
  }
}

Auth.propTypes = {
  
};

export default withStyles(styles)(Auth);