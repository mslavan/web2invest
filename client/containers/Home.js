import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment';
import {Helmet} from "react-helmet";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from '../redux/axiosFunctions' 

import ArticlesFilters from '../components/ArticlesFilters'

const axiosFunc = require('../redux/axiosFunctions') 
const articleCategories = _.sortBy(require('../allData').articleCategories, 'label') 

const styles = {
  
  cardContainer: {
    padding: '0px 10px',
  },

  container: {
    marginTop: 10,
    padding: 10,
    textAlign: 'center',
  },

  firstStepRoot: {
    padding: 10,
  },

  storyRoot: {
    width: '100%',
    padding: 10,
    marginTop: 15,
  },

  divider: {
    margin: '10px 0px',
  },

  cardLogo: {
    paddingTop: 5,
    width: 200,
  },
}

const adminBlogStyle = {

  container: {
    textAlign: 'center',
    padding: 10,
    paddingBottom: 30,
  },

  image: {
    maxWidth: '100%',
    height: 'auto'
  },

  date: {
    color: '#fff',
  },

  header: {
    position: 'relative',
  },

  headerTextContainer: {
    position: 'absolute',
    padding: 15,
    bottom: 4,
    left: 0,    
    width: 'calc(100% - 30px)',
    color: '#f3f3f3',
    background: '#00000080',
  },

}

class Home extends PureComponent {
  
  static fetchData({ store, params }) {
    return Promise.all([
      store.dispatch(actions.getAdminArticlesHome()), 
      store.dispatch(actions.getUsersArticlesHome())
    ])
  }

  constructor(props) {
    super(props);
    
    this.state={
      articles: [],
    }
  }

  componentDidMount(){
    this.props.getUsersArticlesHome();
    this.props.getAdminArticlesHome();
    this.props.setNavLinks([]);
  }

  applyFilters(params){
    this.props.getUsersArticlesHome(params)
  }

	render() {

    const {
      usersArticles,
      adminArticles
    } = this.props
    console.log(this.props)

		return(
          <div>
            
            <Helmet>
              <title>Web2invest - заработок из ничего</title>
            </Helmet>

            <Grid container spacing={0}>
              <Grid item xs={12} md={3} xl={3}>

                <div style={styles.firstStepRoot}>
                  <p style={{ margin: '10px 0px' }}>Работаете в веб-индустрии? Делитесь своими историями</p> 
                  <Link to='/dashboard/stories/new'>Рассказать сейчас</Link>
                </div>

                <div style={styles.firstStepRoot}>
                  <ArticlesFilters update={::this.applyFilters}/>
                </div>

              </Grid>              
              <Grid item xs={12} md={8} xl={8}>

                <h2 className='unstrictly sec__color'>Рекомендовано</h2>

                <Grid container spacing={0}>
                  {adminArticles.map( article =>
                    <Grid item xs={12} sm={6} md={4} xl={4} style={adminBlogStyle.container}>
                      <img src={article.image} alt={article.image} title={article.title} style={adminBlogStyle.image}/>
                      <Link to={`/blog/${article.b_id}`}>
                        <p className='link extra__bold'>{article.title}</p>
                      </Link>
                    </Grid>
                  )}
                </Grid>

                <h2 className='unstrictly sec__color'>Статьи пользователей</h2>
                
                {usersArticles.length
                  ? usersArticles.map( story =>
                      <div key={story.id} style={styles.cardContainer}>
                        <Grid container spacing={0}>
                          <Grid item xs={12} md={4} xl={4}>                    
                            { story.image && 
                              <Link to={`/stories/${story.a_id}`}>
                                <img src={story.image} alt={story.image} title={story.title}  style={styles.cardLogo}/>
                              </Link> }
                          </Grid>
                          <Grid item xs={12} md={8} xl={8}>
                            <p className='unstrictly bold'>
                              {story.category}
                            </p>
                            <Link to={`/stories/${story.a_id}`}><p className='heading link extra__bold'>{story.title}</p></Link>
                            <p>
                              {story.text.length > 100 ? story.text.slice(0,100) + '...' : story.text}
                            </p>
                            <p className='unstrictly bold sec__color'>{ story.anonim === 0 ? story.full_name : 'Анонимно' }</p>
                          </Grid>
                        </Grid>
                        <Divider style={styles.divider}/>
                      </div>
                    )
                  : <div>
                      <p>Ничего не найдено, измените условия поиска</p>
                    </div>
                }
              </Grid>
            </Grid>

          </div>
		)
	}
}

function mapStateToProps(state) {
    return {
        usersArticles: state.asyncData.usersArticles,
        adminArticles: state.asyncData.adminArticles,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Home);