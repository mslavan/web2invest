import React, { Component } from 'react'
import { Switch, Route } from 'react-router'
import { bindActionCreators } from 'redux';
import * as pageActions from '../redux/actions/PageActions';
import { connect } from 'react-redux';
import moment from 'moment'
import { renderRoutes } from 'react-router-config';
import { matchPath } from 'react-router';

import Header from '../components/Header'
import Navigation from '../components/Navigation'
import Footer from '../components/Footer'

moment.locale('ru');

const showNav = (pathname) => {
	let noShowRoutes = [ 
		{ path: '/auth', exact: true },
		{ path: '/register', exact: true },
		{ path: '/dashboard', exact: false },
		{ path: '/root/dashboard', exact: false },
	];
	let foundPath = null;

	let show = !noShowRoutes.find( ({ path, exact }) =>
		matchPath(pathname, { path, exact, strict: true }) || false
	);

	return show;
}

export default class App extends Component {

	constructor(props) {
	   	super(props);
	    
	    this.state={
	      navLinks: [],
	    }
	} 

	componentDidMount() {
		const jssStyles = document.getElementById('jss-server-side');
		
		if (jssStyles && jssStyles.parentNode) {
			jssStyles.parentNode.removeChild(jssStyles);
		}
	}

	setNavLinks(navLinks){
		this.setState({ navLinks })
	}

	render() {
		const pathname = this.props.location.pathname;
		const navLinks = this.state.navLinks;

		return(	
		  <div>
		  	<Header/>
		  	<Navigation show={showNav(pathname)} navLinks={navLinks}/>
		    <main>
          		{renderRoutes(this.props.route.routes, { setNavLinks: ::this.setNavLinks})}
			</main>
		    <Footer/>
		  </div>
		)
	}
}