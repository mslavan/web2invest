import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import ListSubheader from 'material-ui/List/ListSubheader';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Collapse from 'material-ui/transitions/Collapse';
import { bindActionCreators } from 'redux';
import * as pageActions from '../redux/actions/PageActions'
import { connect } from 'react-redux'
import _ from 'lodash'

import AdminBlog from '../components/AdminBlog'
import AdminAddArticle from '../components/AdminAddArticle'

const styles = {
  container: {
    textAlign: 'center',
    padding: 10,
  },
  bigAvatar: {
    margin: 'auto',
    width: 120,
    height: 120,
  },
  rootMenu: {
    textAlign: 'left',
    width: '100%',
    maxWidth: 360,
  },
  nested: {
    paddingLeft: 30,
  },
};

function mapStateToProps(state) {
  return {
    profile: state.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Home extends Component {

  constructor(props) {
    super(props);
    
    this.state={
      openMenuItem: null,
    }
  }

  componentDidMount(){
    if( _.isEmpty(this.props.profile.user) || this.props.profile.user.root !== true ){
      this.props.history.push('/')
    }
  }



  pushUrl(url){
    this.props.history.push(url)
  }

  
  
  render() {
    const user = this.props.profile ? this.props.profile.user : {}
    const path = this.props.location.pathname
    console.log(this.props)

    return(
            <Grid container spacing={24}>
              
              <Grid item xs={12} sm={12} md={5} xl={4}>
                <Paper style={styles.container}>
                  <Avatar alt={user.email} src={ '/images/admin.png'} style={styles.bigAvatar}/>
                  <h3>Администратор</h3>
                  <h3>Емейл: { user.email}</h3>
                  <div>
                    <List style={styles.rootMenu} subheader={<ListSubheader>Разделы</ListSubheader>}>
                      <ListItem button onClick={() => this.pushUrl('/root/dashboard/blog')}>
                        <ListItemIcon>
                          <img src='/images/icons/menu.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Ваш блог" />
                      </ListItem>
                      <ListItem button onClick={() => this.pushUrl('/root/dashboard/blog/add')}>
                        <ListItemIcon>
                          <img src='/images/icons/plus.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Добавить статью в блог"/>
                      </ListItem>
                    </List>
                  </div>
                </Paper>
              </Grid>

              <Grid item xs={12} sm={12} md={7} xl={8}>
                { (path === '/root/dashboard/blog' || path === '/dashboard') && <AdminBlog user={user} history={this.props.history} /> }
                { path === '/root/dashboard/blog/add' && <AdminAddArticle user={user} history={this.props.history} /> }
              </Grid>

            </Grid>

    )
  }
}