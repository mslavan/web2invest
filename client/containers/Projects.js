import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment';
import {Helmet} from "react-helmet";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'material-ui/Icon';
import AccessTime from 'material-ui-icons/AccessTime'

import * as actions from '../redux/axiosFunctions' 

import ProjectsFilters from '../components/ProjectsFilters'

const axiosFunc = require('../redux/axiosFunctions') 
const currencies = require('../allData').currencies;  

const styles = {

  root: {
    padding: 10,
  },

  cardContainer: {
    marginBottom: 15,
    padding: 15,
    borderRadius: 3,
    background: '#fff',
  },

  price: {
    display: 'inline-block',
    borderRadius: 7,
    marginTop: 10,
    padding: '3px 8px',
    color: '#fff',
    background: '#3f51b5'
  },

  date: {
    display: 'inline-block',
  },

  dateIcon: {
    display: 'inline-block',
    marginTop: 5,
  },
}

class Projects extends PureComponent {
  
  static fetchData({ store, params }) {
    return Promise.all([
      store.dispatch(actions.getAllProjects()) 
    ])
  }

  constructor(props) {
    super(props);
    
    this.state={
    }
  }

  componentWillMount(){
    

  }

  componentDidMount(){
    this.props.getAllProjects();
    this.props.setNavLinks([
      { ref: '/projects', title: 'Проэкты' },
    ])  
  }

  applyFilters(params){
    this.props.getAllProjects(params) 
  }

	render() {

    const {
      projects
    } = this.props

		return(
          <div>
            
            <Helmet>
              <title>Проэкты для фрилансеров на сервисе web2invest</title>
            </Helmet>

            <Grid container spacing={0}>
              
              <Grid item xs={12} md={3} xl={3}>
                <ProjectsFilters update={::this.applyFilters}/>
              </Grid>

              <Grid item xs={12} md={9} xl={9}>
                <div style={styles.root}>
                  <p style={{ paddingLeft: 15 }}>Найдено: {projects.length}</p>
                </div>
                <div style={styles.root}>
                {projects.length
                  ? projects.map( project =>
                      <div key={project.pr_id} style={styles.cardContainer}>
                        <Grid container spacing={0}>
                          <Grid item xs={12}>                    
                            <Grid container spacing={0}>
                              <Grid item xs={8} sm={10} xl={10}>
                                <Link to={`/projects/${project.pr_id}`}><p className='heading link extra__bold'>{project.title}</p></Link>
                              </Grid>
                              <Grid item xs={4} sm={2} xl={2} style={{ textAlign: 'right' }}>
                                <p style={styles.price}>
                                  {!project.contract_price 
                                  ? `${project.amount} ${currencies[parseInt(project.currency)].label}`
                                  : `Договорная цена`}
                                </p>
                              </Grid>
                            </Grid>
                            <p>
                              {project.description.length > 100 ? project.description.slice(0,100) + '...' : project.description}
                            </p>
                            <div style={{ position: 'relative' }}>
                              <div>
                                <span>{project.author}</span>
                              </div>
                              <div style={{ display: 'block', width: 'max-content' }}>
                                <AccessTime/> 
                                <p style={{ display: 'block', float: 'right', marginTop: 0 }}>{moment(project.created).format('DD.MM.YYYY')}</p>
                              </div>
                            </div>
                          </Grid>
                        </Grid>
                      </div>
                    )
                  : <div>
                      <p>Ничего не найдено, измените условия поиска</p>
                    </div>
                }
                </div>
              </Grid>

            </Grid>

          </div>
		)
	}
}


function mapStateToProps(state) {
    return {
        projects: state.asyncData.projects,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Projects);