import React, { Component } from 'react'
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import ListSubheader from 'material-ui/List/ListSubheader';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Collapse from 'material-ui/transitions/Collapse';
import { bindActionCreators } from 'redux';
import * as pageActions from '../redux/actions/PageActions'
import { connect } from 'react-redux'
import _ from 'lodash'

import UserAddArticle from '../components/UserAddArticle'
import UserAllArticles from '../components/UserAllArticles'
import UserAccount from '../components/UserAccount'
import UserAddProject from '../components/UserAddProject'
import UserAllProjects from '../components/UserAllProjects'

const styles = {
  container: {
    textAlign: 'center',
    padding: 10,
  },
  bigAvatar: {
    margin: 'auto',
    width: 120,
    height: 120,
  },
  rootMenu: {
    textAlign: 'left',
    width: '100%',
    maxWidth: 360,
  },
  nested: {
    paddingLeft: 30,
  },
};

function mapStateToProps(state) {
  return {
    profile: state.profile
  }
}
function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Home extends Component {

  constructor(props) {
    super(props);
    
    this.state={
      openMenuItem: null,
    }
  }

  componentDidMount(){
    if( _.isEmpty(this.props.profile.user) || this.props.profile.user.root === true ){
      this.props.history.push('/')
    }
  }



  pushUrl(url){
    this.props.history.push(url)
  }

  
	
  render() {
    const user = this.props.profile ? this.props.profile.user : {}
    const path = this.props.location.pathname
    console.log(this.props)

    return(
            <Grid container spacing={24}>
              
              <Grid item xs={12} sm={12} md={5} xl={4}>
                <Paper style={styles.container}>
                  <Avatar alt={`${user.name} ${user.last_name}`} src={ user.avatar || '/images/default_user.png'} style={styles.bigAvatar}/>
                  <h3>{ user.name} {user.last_name }</h3>
                  <div>
                    <List style={styles.rootMenu} subheader={<ListSubheader>Разделы</ListSubheader>}>
                      <ListItem button onClick={() => this.pushUrl('/dashboard/account')}>
                        <ListItemIcon>
                          <img src='/images/icons/clipboard-account.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Аккаунт" />
                      </ListItem>
                      <ListItem button onClick={() => this.pushUrl('/dashboard/stories/new')}>
                        <ListItemIcon>
                          <img src='/images/icons/plus.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Рассказать еще"/>
                      </ListItem>
                      <ListItem button onClick={() => this.pushUrl('/dashboard/stories')}>
                        <ListItemIcon>
                          <img src='/images/icons/menu.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Мои истории"/>
                      </ListItem>
                    </List>
                    <List style={styles.rootMenu} subheader={<ListSubheader>Проэкты (задание)</ListSubheader>}>
                      <ListItem button onClick={() => this.pushUrl('/dashboard/projects/new')}>
                        <ListItemIcon>
                          <img src='/images/icons/plus.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Создать задание"/>
                      </ListItem>
                      <ListItem button onClick={() => this.pushUrl('/dashboard/projects')}>
                        <ListItemIcon>
                          <img src='/images/icons/menu.svg' />
                        </ListItemIcon>
                        <ListItemText inset primary="Мои задания"/>
                      </ListItem>                                       
                    </List>                    
                  </div>
                </Paper>
              </Grid>

              <Grid item xs={12} sm={12} md={7} xl={8}>
                { (path === '/dashboard/account' || path === '/dashboard') && <UserAccount user={user} pageActions={this.props.pageActions} /> }
                { path === '/dashboard/stories/new' && <UserAddArticle user={user} history={this.props.history} /> }
                { path === '/dashboard/stories' && <UserAllArticles user={user}/> }
                { path === '/dashboard/projects/new' && <UserAddProject user={user} history={this.props.history} /> }
                { path === '/dashboard/projects' && <UserAllProjects user={user}/> }
              </Grid>

            </Grid>

		)
	}
}