/* globals ENVIRONMENT */
/* eslint-disable react/no-danger */
import React from 'react';

function renderInitialState(initialState) {
  const innerHtml = `window.INITIAL_STATE = ${JSON.stringify(initialState)}`;
  return <script dangerouslySetInnerHTML={{ __html: innerHtml }} />;
}

function renderEnvironment() {
  const innerHtml = `window.ENVIRONMENT = '${ENVIRONMENT}'`;
  return <script dangerouslySetInnerHTML={{ __html: innerHtml }} />;
}

const Template = ({ content, head, initialState }) => (
  <html lang="ru">
    <head>
      <title>Начните зарабатывать на сайтах с Web2invest </title>
      <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
      <meta name="description" content="Прибыльные сайты на InvestMe" />
      <meta name="keywords" content="Мы расскажем как заработать и куда инвестировать ваши деньги. Научитесь зарабатывать в интернете, тратьте деньги с умом " />
      <meta property="fb:app_id" content="123715308261227"/>
      <meta property="og:title" content="Web2invest - заработок из ничего"/>
      <meta property="og:description" content="Веб - это деньги? Если вы web-developer или продвигаете проэкты, то вам нужно знать Правду."/>
      <meta property="og:image" content="https://web2invest.com/public/images/logo-big.png"/>
      <meta property="og:type" content="article"/>
      <meta property="og:url" content= "https://web2invest.com" />
      
      <meta name="google-site-verification" content="LWNkS_zMben5XXgYCW_djp2vbkfAyldzhFGzPOOPstQ" />
      <link rel="stylesheet" href="/css/style.css" />
      <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow" rel="stylesheet"/>

    </head>
    <body>
      <div id="root" dangerouslySetInnerHTML={{ __html: content }} />
      {initialState && renderInitialState(initialState)}
      <script src={!process.env.NODE_ENV ? '/bundle.js' : '/bundle.min.js'} />
    </body>
  </html>
);

export default Template;
