import React, { PureComponent } from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';

const styles = {
  
  container: {
    margin: 'auto',
    maxWidth: 700,
  },

  title: {
    fontSize: 24,
    marginTop: 10,
  },

  strong: {
    fontWeight: 800,
    fontSize: 26,
    color: '#da8d77',
  },

  font21: {
    fontSize: 21,
  },

  image: {
    maxWidth: '100%',
  }
} 

export default class About extends PureComponent {
    
    componentWillMount(){
      this.props.setNavLinks([
        { ref: '/about', title: 'О нас' },
      ])  
    }
  
    render() {

      return(
          <div style={styles.container}>
            <h2 className='unstrictly sec__color'>Сервис Web2invest всегда с вами</h2>            
            <img src='/images/frameworks.png' style={styles.image}/>
            <p style={styles.font21}>
              Администрация следит за новостями мира веб-разработки, в <Link to='/blog'>своем блоге</Link> мы делимся полезным опытом и знаниями
            </p> 

            <h2 className='unstrictly sec__color'>Планируйте ваш бизнес-сайт с умом</h2>            
            <p style={styles.font21}>    
              Узнайте об опыте ваших товарищей, найдите конкурентов или партнеров. Наши пользователи открыты к общению
            </p>          

            <h2 className='unstrictly sec__color'>Зарабатывать в интернете может каждый! </h2>            
            <img src='/images/dolars-invest.jpg' style={styles.image}/> 
            <p style={styles.font21}> 
              А вы знаете, что сайт <ins>не всегда требует больших вложений</ins>?
              Вы верите, что сайт поможет стать <ins>миллионером</ins>, или же это миф, что на сайте можно сделать состояние?<br/>
              Делитесь своим опытом, найдите ответы на вопросы прямо на нашем портале
            </p>        
          </div>
      )
    }
}
