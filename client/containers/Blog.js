import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment'
import {Helmet} from "react-helmet";

import ModeEditIcon from 'material-ui-icons/ArrowForward';

const axiosFunc = require('../redux/axiosFunctions') 
const articleCategories = _.sortBy(require('../allData').articleCategories, 'label') 

const blogStyle = {
  root: {
    padding: '20px 10px',
  },

  container: {
    position: 'relative',
    padding: 15,
    paddingBottom: 65,
  },

  image: {
    zIndex: 0,
    width: '100%',
  },

  headerTextContainer: {
    padding: 15,
  },

  text: {
  },

}

export default class Blog extends PureComponent {

  constructor(props) {
    super(props);
    
    this.state={
      adminArticles: [],
    }
  }

  componentWillMount(){
    this.getArticles()
    
    this.props.setNavLinks([
      { ref: '/blog', title: 'Блог' },
    ])
  }


  getArticles(){
    axiosFunc
      .getAdminArticles()
      .then( response => {
        response.data.data.map( item => {
          item.text = item.text.replace(/(<([^>]+)>)/ig," ");
          return item;
        })
        
        this.setState({ adminArticles: response.data.data }) 
      })
  }

  pushUrl(url){
    this.props.history.push(url);
  }

	render() {
    console.log(this.state)

		return(
          <div>

            <Helmet>
              <title>Web2invest - блог</title>
            </Helmet>            

            <h1>Блог администрации</h1>

            <Grid container spacing={0}>
              {this.state.adminArticles.map( article =>
                <Grid item xs={12} md={4} xl={4} style={blogStyle.root}>
                  <Paper style={blogStyle.container}>
                    <img src={article.image} style={blogStyle.image}/>
                    <p className='heading extra__bold' onClick={() => ::this.pushUrl(`/blog/${article.b_id}`)}>{article.title}</p>
                    <p style={blogStyle.text}>{article.text.length > 200 ? article.text.slice(0,200) + '...' : article.text}</p>
                    <Button color="primary" aria-label="more" style={{ float: 'right' }} onClick={() => ::this.pushUrl(`/blog/${article.b_id}`)}>
                      Больше <ModeEditIcon/>
                    </Button>                    
                  </Paper>
                </Grid>
              )}
            </Grid>          
          </div>
		)
	}
}