import React, { Component } from 'react'
import { Route } from 'react-router-dom';

const styles = {

	wrapper: {
		margin: 'auto',
		padding: 15,
		textAlign: 'center',
		background: '#cec1b2',
	},

	title: { 
		fontSize: 24,
	},
}

const NotFound = () => {
  return (
    <Route render={({ staticContext }) => {
      if (staticContext) {
        staticContext.status = 404;
      }

      return (
		<div style={styles.wrapper}>
			<h2 style={styles.title}>Ошибка 404</h2>
			<h3>Страница не существует, проверьте корректность адреса</h3>
			<img src='/images/error404.png' />
		</div>
      )
    }}/>
  );
};

export default NotFound;