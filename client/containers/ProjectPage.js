import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment';
import {Helmet} from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';

import * as actions from'../redux/axiosFunctions' 

import SharingSocial from '../components/SharingSocial'
import Disqus from '../components/Disqus'

const currencies = require('../allData').currencies;  

const styles = {

  center: {
    textAlign: 'center',
  },

  articleLogo: {
    maxWidth: '100%',
    height: 'auto',
  },

}


class ProjectPage extends PureComponent {
  
  static fetchData({ store, params }) {
    return store.dispatch(actions.getProjectData(params.id));
  }

  componentDidMount(){
    const paramID = this.props.match.params.id;
    const title = this.props.projectData.title || 'Без названия';

    this.props.getProjectData(paramID);

    this.props.setNavLinks([
      { ref: '/projects', title: 'Проэкты' },
      { ref: `/projects/${paramID}`, title: title },
    ])
  }

  componentWillReceiveProps(nextProps){
    const paramID = nextProps.match.params.id;
    const title = nextProps.projectData.title || 'Без названия';

    if(nextProps.projectData.title !== this.props.projectData.title){
      this.props.setNavLinks([
        { ref: '/projects', title: 'Проэкты' },
        { ref: `/projects/${paramID}`, title: title },
      ])
    }
  }

  render() {
    const projectData = this.props.projectData
    const url  = `https://web2invest.com${this.props.location.pathname}`
    const disqusConfig = {
        identifier: 'web2invest',
        url: `https://web2invest.com${this.props.location.pathname}`,
        title: '',      
    }    
    console.log(this.props)

    return(
          <div style={styles.container}>

            <Helmet>
              <title>{projectData.title}</title>

              <meta property="fb:app_id" content="123715308261227"/>
              <meta property="og:title" content={`${projectData.title || 'Без темы'} - Блог`}/>
              <meta property="og:description" content={projectData.text && projectData.text.replace(/(<([^>]+)>)/ig," ")}/>
              <meta property="og:type" content="article"/>
              <meta property="og:url" content={url} />
            </Helmet>

            <Grid container spacing={24}>

              <Grid item xs={12} md={2} xl={3}>                         
              </Grid>

              <Grid item xs={12} md={8} xl={5}>
                <h1 style={{ wordBreak: 'break-word' }}>{projectData.title}</h1>
                <p>
                  Цена:  
                  {!projectData.contract_price 
                    ? ` ${projectData.amount} ${currencies[parseInt(projectData.currency || 0)].label}`
                    : ` Договорная цена`}
                </p>
                <p> Задание: <br /> {projectData.description}</p>
                
                <Disqus config={disqusConfig} />
              </Grid>

              <Grid item xs={12} md={2} xl={4}>                         
              </Grid>

            </Grid>          
          </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        projectData: state.asyncData.projectData,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(ProjectPage);