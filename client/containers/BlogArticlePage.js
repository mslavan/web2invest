import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import _ from 'lodash';
import moment from 'moment';
import {Helmet} from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';

import * as actions from'../redux/axiosFunctions' 

import SharingSocial from '../components/SharingSocial'
import Disqus from '../components/Disqus'

const styles = {

  container: {
    margin: 'auto',
    maxWidth: 600,
  },

  center: {
    textAlign: 'center',
  },

  articleLogo: {
    maxWidth: '100%',
    height: 'auto',
  },

}


class BlogArticlePage extends PureComponent {
  
  static fetchData({ store, params }) {
    return store.dispatch(actions.getBlogPageData(params.id));
  }

  componentDidMount(){
    const paramID = this.props.match.params.id;
    const title = this.props.blogData.title || 'Без названия';

    this.props.getBlogPageData(paramID);

    this.props.setNavLinks([
      { ref: '/blog', title: 'Блог' },
      { ref: `/blog/${paramID}`, title: title },
    ])
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.blogData.title !== this.props.blogData.title){
      this.props.setNavLinks([
        { ref: '/blog', title: 'Блог' },
        { ref: `/blog/${nextProps.match.params.id}`, title: nextProps.blogData.title },
      ])
    }
  }

	render() {
    const blogData = this.props.blogData
    const url  = `https://web2invest.com${this.props.location.pathname}`
    const disqusConfig = {
        identifier: 'web2invest',
        url: `https://web2invest.com${this.props.location.pathname}`,
        title: '',      
    }

		return(
          <div style={styles.container}>


            <Helmet>
              <title>{blogData.title}</title>

              <meta property="fb:app_id" content="123715308261227"/>
              <meta property="og:title" content={`${blogData.title || 'Без темы'} - Блог`}/>
              <meta property="og:description" content={blogData.text && blogData.text.replace(/(<([^>]+)>)/ig," ")}/>
              <meta property="og:image" content={blogData.image}/>
              <meta property="og:type" content="article"/>
              <meta property="og:url" content={url} />
            </Helmet>

            <Grid container spacing={24}>

              <Grid item xs={12} md={2} xl={3}>                         
                <div style={{ marginTop: 55, marginBottom: 25 }}>
                  <SharingSocial url={url} isStatic={false} />
                </div> 
              </Grid>

              <Grid item xs={12} md={8} xl={5}>
                <div style={styles.center}>
                  <h1 style={{ wordBreak: 'break-word' }}>{blogData.title}</h1>
                  <img src={blogData.image} style={styles.articleLogo}/>
                </div>

                <Grid container spacing={24} style={{ marginBottom: 50 }}>
                  <p className='smaller__text bold'>Опубликовано: {moment(blogData.created).format('DD MMM YYYY')}</p>
                  {blogData.text && renderHTML(blogData.text)}
                </Grid>
                
                <Disqus config={disqusConfig} />
              </Grid>

                <Grid item xs={12} md={2} xl={4}>                         
                </Grid>

            </Grid>

          </div>
		)
	}
}

function mapStateToProps(state) {
    return {
        blogData: state.asyncData.blogData,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(BlogArticlePage);