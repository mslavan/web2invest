import App from './containers/App';
import About from './containers/About';
import Home from './containers/Home';
import Blog from './containers/Blog';
import Projects from './containers/Projects';
import ProjectPage from './containers/ProjectPage';
import BlogPage from './containers/BlogArticlePage';
import Users from './containers/Users';
import UserPage from './containers/UserPage';
import Auth from './containers/Auth';
import Register from './containers/Register';
import StoryPage from './containers/StoryPage';
import UserDashboard from './containers/UserDashboard';
import RootAuth from './containers/RootAuth';
import RootDashboard from './containers/RootDashboard';
import Logout from './containers/Logout';
import NotFound from './containers/NotFound';

const routes = [
  {
    component: App,
    routes: [
      {
        path: '/',
        exact: true,
        component: Home
      },
      {
        path: '/about',
        exact: true,
        component: About
      },
      {
        path: '/blog',
        exact: true,
        component: Blog
      },
      {
        path: '/projects',
        exact: true,
        component: Projects
      },
      {
        path: '/projects/:id',
        exact: true,
        component: ProjectPage
      },
      {
        path: '/blog/:id',
        exact: true,
        component: BlogPage
      },
      {
        path: '/users',
        exact: true,
        component: Users
      },
      {
        path: '/users/:id',
        exact: true,
        component: UserPage
      },
      {
        path: '/auth',
        exact: true,
        component: Auth
      },
      {
        path: '/register',
        exact: true,
        component: Register
      },
      {
        path: '/stories/:id',
        exact: true,
        component: StoryPage,
      },
      {
        path: '/dashboard',
        exact: false,
        component: UserDashboard
      },
      {
        path: '/root/auth',
        exact: true,
        component: RootAuth
      },
      {
        path: '/root/dashboard',
        exact: false,
        component: RootDashboard
      },
      {
        path: '/logout',
        exact: true,
        component: Logout
      },
      {
        path: '*',
        exact: true,
        component: NotFound,
      },
    ]
  }
];

export default routes;