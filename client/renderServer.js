import React from 'react';


function renderInitialState(preloadedState) {
  return `window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}`;
}


export default (content, helmet, css, preloadedState) => `<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
      <meta name="description" content="Web2invest - заработок из ничего" />
      <meta name="keywords" content="Мы расскажем как заработать и куда инвестировать ваши деньги. Научитесь зарабатывать в интернете, тратьте деньги с умом " />
      
      ${helmet.title.toString()}
      ${helmet.meta.toString()}
      ${helmet.link.toString()}    

      <meta name="google-site-verification" content="LWNkS_zMben5XXgYCW_djp2vbkfAyldzhFGzPOOPstQ" />
      <link rel="stylesheet" href="/css/style.css" />
      <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,700|Poiret+One" rel="stylesheet" />

      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <script>
        (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7999459924450561",
          enable_page_level_ads: true
        });
      </script>
      
    </head>
    <body>
      
      <div id="root">${content}</div>
      <style id="jss-server-side">${css}</style>
      <script>${preloadedState && renderInitialState(preloadedState)}</script>
      <script src=${process.env.NODE_ENV !== 'production' ? '/bundle.js' : '/bundle.min.js'}></script>

    </body>
</html>`;