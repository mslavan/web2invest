
const articleCategories = [
	{ id: 0, label: 'Другое' },
	{ id: 1, label: 'Бизнес' },
	{ id: 2, label: 'Дети' },
	{ id: 3, label: 'Дом' },
	{ id: 4, label: 'Досуг' },
	{ id: 5, label: 'Здоровье' },
	{ id: 6, label: 'Игры' },
	{ id: 7, label: 'Интернет' },
	{ id: 8, label: 'Искуство' },
	{ id: 9, label: 'Компьютеры' },
	{ id: 10, label: 'Наука' },
	{ id: 11, label: 'Новости и СМИ' },
	{ id: 12, label: 'Образование' },
	{ id: 13, label: 'Общество' },
	{ id: 14, label: 'Покупки' },
	{ id: 15, label: 'Спорт' },
	{ id: 16, label: 'Туризм' },
];

const currencies = [
	{ value: 0, label: '$', title: 'USD' },
	{ value: 1, label: '€', title: 'EUR' },
	{ value: 2, label: '₽', title: 'RUB' },
	{ value: 3, label: '₴', title: 'UAH' },
]

module.exports = {
  articleCategories,
  currencies,
}