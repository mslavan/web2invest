import React, {Component} from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Button from 'material-ui/Button';

const axiosFunc = require('../redux/axiosFunctions') 

const regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const styles = {
  root: {
    width: '100%',
  },

  innerContainer: {
    padding: 10,
    background: '#fff',
  },
};

const resetForm = {
  storyText: '',
}

export default class UserAccount extends Component {
    
  constructor(props) {
    super(props);
    
    this.state={
      name: this.props.user.name,
      last_name: this.props.user.last_name,
      email: this.props.user.email,
      password: this.props.user.password,  
    }
  }


  handleChange(event){
    this.setState({
      [name]: event.target.value,
    });
  }

  updateProfile(){

    if(this.state.name && this.state.last_name && regExpEmail.test(this.state.email) && this.state.password) {
      axiosFunc
        .updateUserAccount( 
          this.props.user.id, 
          this.state.name, 
          this.state.last_name,
          this.state.email,
          this.state.password,
        )
        .then( (response) => {
          this.props.pageActions.loginUser(response.data.id_token)
          this.setState({ changingData: false })
        })    
    }
  }

  render() {
    console.log(this.props);

    return (  

       <div style={styles.root}>
       		<h3>Ваш аккаунт</h3>
          <Paper style={styles.innerContainer}>
            
            <Grid container>
              <Grid item xs={6}>            
                <TextField
                  label='Имя'
                  disabled={!this.state.changingData}
                  fullWidth
                  value={this.state.name}
                  onChange={(e) => this.setState({ name: e.target.value })}
                  margin='normal' 
                />            
              </Grid>            
              <Grid item xs={6}>            
                <TextField
                  label='Фамилия'
                  disabled={!this.state.changingData}
                  fullWidth
                  value={this.state.last_name}
                  onChange={(e) => this.setState({ last_name: e.target.value })}
                  margin='normal'
                />
              </Grid>
            </Grid>

            <Grid container>
              <Grid item xs={6}>            
                <TextField
                  label='Email'
                  disabled={!this.state.changingData}
                  fullWidth
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  margin='normal' 
                />            
              </Grid>            
              <Grid item xs={6}>            
                <TextField
                  label='Пароль'
                  disabled={!this.state.changingData}
                  fullWidth
                  type='password'
                  value={this.state.password}
                  onChange={(e) => this.setState({ password: e.target.value })}
                  margin='normal'
                />

              </Grid>
            </Grid>          
            
            <div style={{ textAlign: 'right', height: 50 }}>
              { this.state.changingData 
                ? <Button raised onClick={::this.updateProfile}>
                    Обновить
                  </Button> 
                : <Button raised onClick={() => this.setState({ changingData: true })}>
                    Изменить
                  </Button> 
              }

            </div>
          </Paper>   
      </div>

    )
  }
}