import React, { Component } from 'react';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import {
  FacebookShareCount,
  FacebookShareButton,
  TwitterShareButton,
  TwitterIcon,
  FacebookIcon
} from 'react-share';


const styles = {
  
  staticRoot: {
  	position: 'relative',
  },

  nonStaticRoot: {
  	position: 'fixed',
  },

  shareTitle: {
    fontWeight: 700,
    fontSize: 24,
  },

  shareButton: {
  	padding: 8,
  },

  shareCount: {
  	textAlign: 'center',
  },
}

const SharingSocial = ({ url, isStatic = false , hiddenDispl = { xsDown: true, smDown: true } }) => 
 <div style={isStatic ? styles.staticRoot : styles.nonStaticRoot}>
	{ isStatic && <div>
					<p style={styles.shareTitle}>Поделиться этой записью</p>
					<hr />
				  </div>}

	<Grid hidden={hiddenDispl} style={{ textAlign: 'center' }}>
		<FacebookShareCount url={url}>
		  {shareCount => (
		    <span style={styles.shareCount} className='heading'>{shareCount}</span>
		  )}
		</FacebookShareCount>

		<FacebookShareButton url={url} style={styles.shareButton}>
			<Button fab mini>
				<FacebookIcon size={42} round />
			</Button>
		</FacebookShareButton>
		<TwitterShareButton url={url} style={styles.shareButton}>
			<Button fab mini>
				<TwitterIcon size={42} round />
			</Button>
		</TwitterShareButton>
	</Grid>
 </div>

export default SharingSocial;