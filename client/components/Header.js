import React, { PureComponent } from 'react';
import Grid from 'material-ui/Grid';
import { string } from 'prop-types';
import { Link } from 'react-router-dom';
import Button from 'material-ui/Button';
import Menu, { MenuItem } from 'material-ui/Menu';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { LinearProgress } from 'material-ui/Progress';
import { bindActionCreators } from 'redux';
import * as pageActions from '../redux/actions/PageActions';
import { connect } from 'react-redux';
import _ from 'lodash';

import ArrowDropDown from 'material-ui-icons/ArrowDropDown';

const styles = {
  
  progressRoot: {
    position: 'fixed',
    width: '100%',
    height: 2,
    zIndex: 1,
    left: 0,
    top: 0,
  },

  wrapper: {
    margin: '0 auto',
    padding: 0,
    width: '100%',
    maxWidth: 1110,
  },

  logo: {
    textDecoration: 'none',
    fontSize: 28,
    margin: 0,
    padding: 0,
    color: '#f3f3f3',
  },

  rightFloat: {
    float: 'right',
  },

  link: {
    color: '#5d6b63',
    textDecoration: 'none',
  },

  userName: {
    margin: 0,
  },

  userNameIcon: {
    display: 'block',
    float: 'right',
    paddingLeft: 5,
    paddingTop: 5,
  }
} 


class Header extends PureComponent {
   
  constructor(props) {
    super(props);
    
    this.state = {
      openUserMenu: false,
      pageScrollY: 0,
    }
  }  

  componentWillMount(){
    console.log(this.props)
  }

  componentDidMount(){
    window.onscroll = ::this.onPageScroll 
  }

  onPageScroll(ev){
      this.setState({ pageScrollY: window.scrollY / (document.body.scrollHeight - window.innerHeight + 98)* 100 })
  }

  componentWillReceiveProps(newProps){
    this.setState({ user: newProps.profile.user })
  }

  render() {
    const user = this.props.profile.user || {}

    return (
          <header> 
              { Boolean(this.state.pageScrollY) && false && 
                <LinearProgress color="accent" mode="indeterminate" value={this.state.pageScrollY} style={styles.progressRoot}/> }

              <Toolbar style={styles.wrapper}>
                <Grid container spacing={24}>
                  
                  <Grid item xs={3}>
                    <Link to='/' style={styles.link}>
                      <h1 style={styles.logo}>Web2invest</h1>
                    </Link>
                  </Grid>

                  <Grid item xs={9}>
                    <div style={Object.assign(styles.rightFloat, {margin: 0, marginRight: 15 }) }>
                        <h2 
                          style={styles.userName}
                          onClick={ (e) => this.setState({ openUserMenu: true, anchorEl: e.currentTarget }) }
                        >
                          {user.name || (user.root && 'Администратор') || 'Гость'}
                          <ArrowDropDown style={styles.userNameIcon}/>
                        </h2>
                    </div>

                    <Menu
                      id="simple-menu"
                      anchorEl={this.state.anchorEl}
                      open={this.state.openUserMenu}
                      onClose={ () => this.setState({ openUserMenu: false }) }
                    >
                      {_.isEmpty(user) && <Link to='/register' ><MenuItem onClick={() => this.setState({ openUserMenu: false })}>Регистрация</MenuItem></Link> }
                      {_.isEmpty(user) && <Link to='/auth' ><MenuItem onClick={() => this.setState({ openUserMenu: false })}>Вход</MenuItem></Link> }
                      {!_.isEmpty(user) && <Link to={user.root ? '/root/dashboard' : '/dashboard'}><MenuItem onClick={() => this.setState({ openUserMenu: false })}>Аккаунт</MenuItem></Link> }
                      {!_.isEmpty(user) && <Link to='/logout' ><MenuItem onClick={() => this.setState({ openUserMenu: false })}>Выйти</MenuItem></Link> }
                    </Menu>

                  </Grid>

                </Grid>
              </Toolbar>
          </header>
    );
  }
}

const mapStateToProps = ({ profile }) => ({
  profile,
});

export default connect(mapStateToProps)(Header);