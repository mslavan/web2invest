import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash'

const styles = {

	root: {
		marginTop: 65,
		height: 45,
	},

	innerRoot: {
		margin: 'auto',
		paddingTop: 12,
		maxWidth: 1110,
		height: 45,
	},

	leftBlock: {
		float: 'left',
	},

	rightBlock: {
		float: 'right',
	},

	navLinkContainer: {
		display: 'inline-block',
		marginLeft: 10,
	},

	section: {
		margin: 10,
	},
};

const Navigation = ({ show, navLinks }) => {
	return (
		
		<nav style={Object.assign(styles.root, show ? {background: '#fff'} : {background: 'none'})}>
			{show && <div style={styles.innerRoot}>
						<div style={styles.leftBlock}>
							{ !_.isEmpty(navLinks) && <Link to='/'>Главная</Link> }
							{navLinks.map( ({ref, title}, index) => 
								<div style={styles.navLinkContainer}>
									<span style={{ fontSize: 18, padding: 5 }}> / </span>
									{index !== navLinks.length -1 
										? <Link to={ref}>{title}</Link> 
										: <span className='bold'>{title}</span>
									} 
								</div>
							)}
						</div>

						<div style={styles.rightBlock}>
							<Link to='/blog' style={styles.section}>Блог</Link>
							<Link to='/users' style={styles.section}>Пользователи</Link>
							<Link to='/projects' style={styles.section}>Проэкты</Link>
							<Link to='/about' style={styles.section}>О нас</Link>
						</div>
					</div>}
		</nav>
	)
}

export default Navigation;