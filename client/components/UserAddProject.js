import React, {Component} from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel, FormControl, FormHelperText } from 'material-ui/Form';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import Switch from 'material-ui/Switch';

import Button from 'material-ui/Button';
import _ from 'lodash';

const axiosFunc = require('../redux/axiosFunctions'); 
const currencies = require('../allData').currencies; 

const styles = {
  root: {
    width: '100%',
  },

  innerContainer: {
    padding: 10,
    background: '#fff',
  },
};

const resetForm = {
  title: '',
  description: ''
}

export default class UserAddProject extends Component {
  
  constructor(props) {
    super(props);
    
    this.state={
      anonimStory: false,
      currency: 0,
      isContractPrice: false,
    }
  }

  handleChange(panel){
    return (event) => {
      this.setState({
        [name]: event.target.value,
      });      
    }
  }


  postProject(){

    const state = this.state;

    if(state.title && state.description && (state.amount && state.currency > -1) || state.isContractPrice){

      axiosFunc
        .postProject( 
          this.props.user.id, 
          state.title, 
          state.description,
          state.currency,
          state.amount,
          state.isContractPrice
        )
        .then( (response) => {
          this.setState(resetForm)
          this.props.history.push('/dashboard/projects')
        })  
    }
  }


  render() {
    console.log(this.state);

    return (  

       <div style={styles.root}>
       		<h3>Новое задание</h3>
          <Paper style={styles.innerContainer}>

            <TextField
              label='Название'
              multiline
              fullWidth
              rowsMax='10'
              value={this.state.title}
              onChange={(e) => this.setState({ title: e.target.value })}              
              margin='normal'
            />

            <TextField
              label='Описание'
              multiline
              fullWidth
              rowsMax='10'
              value={this.state.description}
              onChange={(e) => this.setState({ description: e.target.value })}              
              margin='normal'
            />

            <FormControlLabel
              control={
                <Switch
                checked={this.state.isContractPrice}
                onChange={(e) => this.setState({ isContractPrice: e.target.checked })}
                color="primary"
                />
              }
              label="Договорная цена"
            /><br/>

            {!this.state.isContractPrice &&
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <TextField
                  id="select-currency"
                  select
                  label="Валюта"
                  value={this.state.currency}
                  onChange={(e) => this.setState({ currency: e.target.value })}
                  margin="normal"
                >
                  {currencies.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>              
              </Grid>
              <Grid item xs={6}>
                <FormControl margin="normal">
                  <InputLabel htmlFor="amount">Сумма оплаты</InputLabel>
                  <Input
                    id="adornment-amount"
                    value={this.state.amount}
                    onChange={(e) => this.setState({ amount: e.target.value })}
                    startAdornment={
                      <InputAdornment position="start">{currencies[this.state.currency].label}</InputAdornment>
                    }
                  />
                </FormControl>              
              </Grid>
            </Grid>}

            <FormControlLabel 
              control={
                <Checkbox value="checkedC" onChange ={(e,checked) => this.setState({ anonimStory: checked })} />
              } 
              label='Опубликовать анонимно' />
            
            <div style={{ float: 'right' }}>
              <Button raised onClick={::this.postProject}>
                Поделиться
              </Button>
            </div>
          </Paper>   
      </div>

    )
  }
}