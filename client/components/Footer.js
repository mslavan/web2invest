import React from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router-dom';
import Button from 'material-ui/Button';

const styles = {
  
  wrapper: {
    margin: 'auto',
    width: '100%',
    maxWidth: 1110,
    textAlign: 'center',
    color: '#fff',
  },

  link: {
    color: '#5d6b63',
    textDecoration: 'none',
  },

  whiteColor: {
    color: '#fff',
  },
} 

const Footer = () => {
    return(
        <footer>
          <Grid container style={styles.wrapper}>
                    
            <Grid item xs={12} sm={6} md={4} xl={4}>
              <h2>Разделы</h2>

              <Link to='/' style={styles.whiteColor}>На главную</Link> <br/>
              <Link to='/blog' style={styles.whiteColor}>Блог</Link> <br/>
              <Link to='/register' style={styles.whiteColor}>Зарегистрироваться</Link> <br/>
              <Link to='/auth' style={styles.whiteColor}>Авторизоваться</Link> <br/>
              <Link to='/about' style={styles.whiteColor}>О сервисе</Link> <br/>
            </Grid>

            <Grid item xs={12} sm={6} md={4} xl={4}>
              <h2>Контакты</h2>
              Почта: web2invest@gmail.com
            </Grid>

            <Grid item xs={12} sm={6} md={4} xl={4}>
              <h2>Соц. активность</h2>
                <Button size="small" > 
                  <a href='https://www.facebook.com/groups/581026635585080/' target='blank'>
                    <img src='/images/facebook-white.png'/>
                  </a>
                </Button>
                <Button size="small" >
                  <img src='/images/twitter-white.png' />
                </Button>
            </Grid>

          </Grid>
          <Grid container style={styles.wrapper}>
                    
            <Grid item xs>
              <p style={styles.whiteColor}> Aвторские права ©. Все права защищены Web2invest 2018</p>
            </Grid>
          
          </Grid>
        </footer>
    )
}

export default Footer;