import React, {Component} from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel, FormControl } from 'material-ui/Form';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';

import Button from 'material-ui/Button';
import _ from 'lodash';

const axiosFunc = require('../redux/axiosFunctions') 
const articleCategories = _.sortBy(require('../allData').articleCategories, 'label') 

const styles = {
  root: {
    width: '100%',
  },

  innerContainer: {
    padding: 10,
    background: '#fff',
  },
};

const resetForm = {
  storyText: '',
}

export default class UserAddArticle extends Component {
  
  constructor(props) {
    super(props);
    
    this.state={
      anonimStory: false,
      category: '',
    }
  }

  handleChange(panel){
    return (event) => {
      this.setState({
        [name]: event.target.value,
      });      
    }
  }


  postArticle(){

    if(this.state.storyText && this.state.storyTitle && this.state.category && this.state.uploadedImage){
      
      axiosFunc
        .saveImageRemote(this.state.uploadedImage)
        .then( res => 
          axiosFunc
            .postStory( 
              this.props.user.id, 
              this.state.storyText, 
              this.state.storyTitle , 
              this.state.anonimStory ? 1 : 0, 
              this.state.category, 
              res.data.data.link,
            )
            .then( (response) => {
              this.setState(resetForm)
              this.props.history.push('/dashboard/stories')
            })  
        ) 
    }
  }


  uploadImage(e){
    let reader = new FileReader();
    let file = e.target.files[0];

    if (!file) return;

    reader.onload = function(img) {
      const data = new FormData();
      let uploadedImage = img.target.result.replace(/^[^,]*,/,'')

      this.setState({ uploadedImage, imgData: file })
    }.bind(this);
    reader.readAsDataURL(file);
  }


  render() {
    console.log(this.state);

    return (  

       <div style={styles.root}>
       		<h3>Новая история</h3>
          <Paper style={styles.innerContainer}>
            <FormControl style={{width: '100%'}}>
              <InputLabel htmlFor="category">Тематика</InputLabel>
              <Select
                value={this.state.category}
                onChange={(e) => this.setState({category: e.target.value})}           
                inputProps={{
                  name: 'category',
                  id: 'category',
                }}
              >
                {articleCategories.map(category => (
                  <MenuItem
                    key={category.id}
                    value={category.id}
                  >
                    {category.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <TextField
              label='Название статьи'
              multiline
              fullWidth
              rowsMax='10'
              value={this.state.storyTitle}
              onChange={(e) => this.setState({ storyTitle: e.target.value })}              
              margin='normal'
            />

            <TextField
              label='Текст'
              multiline
              fullWidth
              rowsMax='10'
              value={this.state.storyText}
              onChange={(e) => this.setState({ storyText: e.target.value })}              
              margin='normal'
            />
            
            <input className='hidden' id='in' type='file' accept='image/*' onChange={ ::this.uploadImage } />
            <label htmlFor='in'>
              <p>{this.state.imgData ? `Изображение: ${this.state.imgData.name}` : 'Загрузить изображение'}</p>
            </label>

            <FormControlLabel 
              control={
                <Checkbox value="checkedC" onChange ={(e,checked) => this.setState({ anonimStory: checked })} />
              } 
              label='Опубликовать анонимно' />
            
            <div style={{ float: 'right' }}>
              <Button raised onClick={::this.postArticle}>
                Поделиться
              </Button>
            </div>
          </Paper>   
      </div>

    )
  }
}