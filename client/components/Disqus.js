import React, { Component } from 'react'
import _ from 'lodash'

export default class Disqus extends Component {
 
  componentDidMount(){
  	this.initDiscuss();
  }


  componentWillReceiveProps(nextProps){
  	if(!_.isEqual(nextProps.config !== this.props.config)){
  		this.updateDiscuss(nextProps.config);
  	}
  }


  updateDiscuss(config){

    if(!window.DISQUS) {
      this.initDiscuss()
    }else {
      window.DISQUS.reset({
        reload: true,
        config: config,
      });
    }
  }


  initDiscuss(){

      var script = document.createElement( 'script' );
      script.type = 'text/javascript';
      script.src = `https://trust-to-me.disqus.com/embed.js`;
      script.setAttribute('data-timestamp', +new Date());
      (document.head || document.body).appendChild(script);  
  }


	render() {

		return(
        	<div id="disqus_thread" />
        )
	}
}