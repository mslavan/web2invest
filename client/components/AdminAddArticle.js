import React, {Component} from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel, FormControl } from 'material-ui/Form';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import Button from 'material-ui/Button';
import _ from 'lodash';
import {Helmet} from 'react-helmet';

import TextFieldEditor from './TextFieldEditor'

const axiosFunc = require('../redux/axiosFunctions') 
const articleCategories = _.sortBy(require('../allData').articleCategories, 'label') 

const styles = {
  root: {
    width: '100%',
  },

  innerContainer: {
    padding: 10,
    background: '#fff',
    paddingBottom: 50,
  },
};

const resetForm = {
  storyText: '',
  storyTitle: '',
}

export default class UserAddArticle extends Component {
  
  constructor(props) {
    super(props);
    
    this.state={
      anonimStory: false,
      categories: [],    
    }
  }

  handleChange(panel){
    return (event) => {
      this.setState({
        [name]: event.target.value,
      });      
    }
  }

  postArticle(){

    if(this.state.storyText && this.state.storyTitle && this.state.categories.length && this.state.uploadedImage){
      
      axiosFunc
        .saveImageRemote(this.state.uploadedImage)
        .then( res => 
          axiosFunc
            .postAdminArticle( this.state.categories, this.state.storyText, this.state.storyTitle, res.data.data.link)
            .then( (response) => {
              this.setState(resetForm)
              this.props.history.push('/root/dashboard/blog')
            })
        ) 
    }
  }


  uploadImage(e){
    let reader = new FileReader();
    let file = e.target.files[0];

    if (!file) return;

    reader.onload = function(img) {
      const data = new FormData();
      let uploadedImage = img.target.result.replace(/^[^,]*,/,'')

      this.setState({ uploadedImage, imgData: file })
    }.bind(this);
    reader.readAsDataURL(file);
  }


  render() {
    console.log(this.state);

    return (  

       <div style={styles.root}>


          <Helmet>
            <link rel="stylesheet" href="/css/richEditor.css" />
          </Helmet>  
               
       		<h3>Новая статья</h3>
          <Paper style={styles.innerContainer}>
            <FormControl style={{width: '100%'}}>
              <InputLabel htmlFor="category">Тематика</InputLabel>
              <Select
                multiple
                value={this.state.categories}
                onChange={(e) => this.setState({categories: e.target.value})}
                input={<Input id="name-multiple" style={{ minWidth: 250 }}/>}

              >
                {articleCategories.map(category => (
                  <MenuItem
                    key={category.id}
                    value={category.id}
                    style={{
                      fontWeight: this.state.categories.indexOf(category.id) !== -1 ? '500' : '400',
                    }}
                  >
                    {category.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <TextField
              label='Заголовок'
              multiline
              fullWidth
              rowsMax='10'
              value={this.state.storyTitle}
              onChange={(e) => this.setState({ storyTitle: e.target.value })}              
              margin='normal'
            />

            <TextFieldEditor onChange={(storyText) => this.setState({storyText})}/>

            <input className='hidden' id='in' type='file' accept='image/*' onChange={ ::this.uploadImage } />
            <label htmlFor='in'>
              <p>{this.state.imgData ? `Изображение: ${this.state.imgData.name}` : 'Загрузить изображение'}</p>
            </label>

            <div style={{ float: 'right' }}>
              <Button raised onClick={::this.postArticle}>
                Добавить
              </Button>
            </div>
          </Paper>   
      </div>

    )
  }
}