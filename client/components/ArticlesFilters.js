import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';
import Button from 'material-ui/Button';
import _ from 'lodash'

const styles = {

	root: {
		padding: 10,
	},
};

export default class ArticlesFilters extends Component {
   
	constructor(props) {
		super(props);

		this.state = {
			noAnonims: false,
		}
	}  

	render(){


		return (
			
			<Paper style={styles.root}>
				<p>Фильтры</p>

				<FormControlLabel
					control={
						<Switch
							checked={this.state.noAnonims}
							onChange={(e) => this.setState({ noAnonims: e.target.checked })}
							value="noAnonims"
						/>
					}
					label="Только с авторами" />

				<Button raised fullWidth color="primary" onClick={() => this.props.update(this.state)}>
					Поиск
				</Button>
			</Paper>
		)
	}
}

