import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';
import Button from 'material-ui/Button';
import _ from 'lodash'

const styles = {

	root: {
		padding: 10,
	},
};

export default class ProjectsFilters extends Component {
   
	constructor(props) {
		super(props);

		this.state = {
			noContractPrice: false,
		}
	}  

	render(){


		return (
			
			<Paper style={styles.root}>
				<p>Фильтры</p>

				<FormControlLabel
					control={
						<Switch
							checked={this.state.noContractPrice}
							onChange={(e) => this.setState({ noContractPrice: e.target.checked })}
							value="noContractPrice"
						/>
					}
					label="Указана цена" />

				<Button raised fullWidth color="primary" onClick={() => this.props.update(this.state)}>
					Поиск
				</Button>
			</Paper>
		)
	}
}

