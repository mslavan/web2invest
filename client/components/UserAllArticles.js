import React, {Component} from 'react';
import { string } from 'prop-types';
import Grid from 'material-ui/Grid';
import { Link } from 'react-router';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import ExpansionPanel, {
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ExpansionPanelActions,
} from 'material-ui/ExpansionPanel';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';

const axiosFunc = require('../redux/axiosFunctions') 

const styles = {
  root: {
    width: '100%',
  },

  heading: {
    fontSize: 15,
    flexBasis: '33.33%',
    flexShrink: 0,
  },

  secondaryHeading: {
    fontSize: 15,
  },

};


export default class UserAllArticles extends Component {
  
  constructor(props) {
    super(props);
    
    this.state={
      stories: [],
      openDeleting: false,
      deletingId: null,
    }
  }  


  componentWillMount(){
    this.getStories()
  }

  getStories(){
    axiosFunc
      .getStoriesOneAuthor(this.props.user.id)
      .then( response => this.setState({ stories: response.data.data }) )
  }

  handleChange(panel){
    return (event, expanded) => {
      this.setState({
        expanded: expanded ? panel : false,
      });
    }
  }

  deleteArticle(){
    axiosFunc
      .getDeleteStory(this.state.deletingId)
      .then( response => {
        this.getStories() 
        this.setState({ openDeleting: false, deletingId: null })
      })  
  }

  render() {
    const { expanded } = this.state;
    
    const { 
      user,
      fullScreen 
    } = this.props.user

    console.log(this.state);

    return (  

       <div style={styles.root}>
       		<h3>Ваши истории</h3>
          <div style={styles.root}>
          { this.state.stories.map( story =>
              <ExpansionPanel expanded={expanded === story.a_id} onChange={this.handleChange(story.a_id)}>
                <ExpansionPanelSummary expandIcon={<img src='/images/icons/menu-down.svg' />}>
                  <Typography style={styles.heading}>{ story.anonim === 0 ? 'Публично' : 'Анонимно' }</Typography>
                  <Typography style={styles.secondaryHeading}>{story.title.length > 30 ? story.title.slice(0,30) + '...' : story.title}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <div style={{ width: '100%' }}>
                    <img src={story.image} style={{ width: '100%', height: 'auto' }} />
                  </div>
                  <Typography>
                    <p>{story.text}</p>
                  </Typography>
                </ExpansionPanelDetails>
                <Divider />
                <ExpansionPanelActions>
                  <Button color="primary" onClick={() => this.setState({ openDeleting: true, deletingId: story.a_id })}>Удалить</Button>
                  <Button>
                    Перейти
                  </Button>
                </ExpansionPanelActions>
              </ExpansionPanel>
          )}
          </div>  

          <Dialog
            fullScreen={fullScreen}
            open={this.state.openDeleting}
            onClose={() => this.setState({ openDeleting: false, deletingId: null })}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogTitle id="responsive-dialog-title">Вы действительно хотите удалить запись?</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Это действие невозможно вернуть.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => this.setState({ openDeleting: false, deletingId: null })} color="primary">
                Назад
              </Button>
              <Button onClick={::this.deleteArticle} color="primary" autoFocus>
                Удалить
              </Button>
            </DialogActions>
          </Dialog>

      </div>

    )
  }
}