/* global window, document */
import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import { renderToString } from 'react-dom/server';
import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { MuiThemeProvider, createMuiTheme, createGenerateClassName } from 'material-ui/styles';
import { BrowserRouter, StaticRouter } from 'react-router-dom';
import { matchPath } from 'react-router';
import { matchRoutes, renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux'
import Cookies from 'universal-cookie'
import jwt_decode from 'jwt-decode';
import helmet from 'react-helmet';
import { createStore, applyMiddleware } from 'redux';
import reducers from './redux/reducers';
import thunk from './redux//middleware/thunk';

import renderServer from './renderServer';
import routes from './Routes'
import store from './redux/store'

import ScrollToTop from './ScrollToTop'
import App from './containers/App';


const muiTheme = require('./mui-theme').default;
const isClient = typeof document !== 'undefined';


if (isClient) {
  ReactDOM.render(
    <MuiThemeProvider theme={muiTheme}>
        <Provider store={store}> 
          <BrowserRouter> 
            <ScrollToTop>   
              {renderRoutes(routes)}
            </ScrollToTop>
          </BrowserRouter>
        </Provider>
    </MuiThemeProvider>
    , document.getElementById('root'));
}

const serverMiddleware = async(req, res) => {

  const cookies = new Cookies(req.headers.cookie);
  const store = createStore(reducers, {}, applyMiddleware(thunk));
  const sheetsRegistry = new SheetsRegistry();
  const generateClassName = createGenerateClassName();

  let foundPath = null;

  let { path, component } = routes[0].routes.find(
    ({ path, exact }) => {
      foundPath = matchPath(req.url,
        {
          path,
          exact,
          strict: true
        }
      )
      return foundPath;
    }) || {};

  if (!component)
    component = {};
  
  if (!component.fetchData)
    component.fetchData = () => new Promise(resolve => resolve());

  await component.fetchData({ store, params: (foundPath ? foundPath.params : {}) });
  
  let preloadedState = store.getState();
  preloadedState.profile.user = cookies.get('InvestUser') ? jwt_decode(cookies.get('InvestUser')) : {}

  let context = {};

  const content = renderToString(
    <Provider store={store}>
      <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
        <MuiThemeProvider theme={muiTheme} sheetsManager={new Map()}>   
          <StaticRouter location={req.url} context={context}>
            {renderRoutes(routes)}
          </StaticRouter>
        </MuiThemeProvider>
      </JssProvider>
    </Provider>
  );

  const head = helmet.renderStatic();
  const css = sheetsRegistry.toString();

  if (context.status === 404) {
    res.status(404);
  }
  if (context.status === 302) {
    return res.redirect(302, context.url);
  }

  res.send(renderServer(content, head, css, preloadedState));
}

export default serverMiddleware;
